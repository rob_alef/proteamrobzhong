package com.uchicago.ralef.services;

import com.sun.jersey.spi.resource.Singleton;
import com.uchicago.ralef.model.Pet;


import javax.ws.rs.*;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by Robert on 7/27/2015.
 */
@Produces("application/xml")
@Path("petservices")
@Singleton
public class PetResource {

    private TreeMap<Integer, Pet> petMap = new TreeMap<>();

    /**
     * Stores some objects in our petMap
     */
    public PetResource(){

        Pet anniePet = new Pet();
        anniePet.setName("Annie");
        anniePet.setAnimalType("Dog");
        anniePet.setWeight(75);

        Pet busterPet = new Pet();
        busterPet.setName("Buster");
        busterPet.setAnimalType("Dog");
        busterPet.setWeight(10);

        Pet daisyPet = new Pet();
        daisyPet.setAnimalType("Daisy");
        daisyPet.setAnimalType("Snake");
        daisyPet.setWeight(200);

        addPet(anniePet);
        addPet(busterPet);
        addPet(daisyPet);

    }

    //TODO get to get to the add page.


    @POST
    @Path("add")
    @Consumes("application/xml")
    public String addPet(Pet pet){
        int id = petMap.size();
        pet.setId(id);
        petMap.put(id, pet);
        return "Pet Added!! " + pet.toString();
    }

    @GET
    @Path("{name}")
    public Pet showPet(@PathParam("name") String name){

        Pet foundPet = null;
        for (Pet yourPet : petMap.values()){

            if (foundPet.getName().equals(name)){
                foundPet = yourPet;
                break;
            }
        }
        return foundPet;
    }

    @GET
    public List<Pet> getPets(){
        List<Pet> pets = new ArrayList<>();
        pets.addAll(petMap.values());
        return pets;
    }

    @DELETE
    @Path("delete/{id}")
    @Produces("text/html")
    public String deletePet(@PathParam("id") int id){
        petMap.remove(id);
        return "Pet removed";
    }

    //TODO GET to get to the edit page.

    @PUT
    @Path("update")
    @Produces("text/html")
    @Consumes("application/xml")
    public String updatePet(Pet pet){
        petMap.put(pet.getId(), pet);
        return "Pet updated " + pet.toString();
    }
    //TODO DO API METHOD!!
}
