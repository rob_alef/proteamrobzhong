package com.uchicago.ralef.model;

/**
 * Created by Robert on 7/27/2015.
 */
public class Pet {

    private String name;
    private String animalType;
    private int weight;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnimalType() {
        return animalType;
    }

    public void setAnimalType(String animalType) {
        this.animalType = animalType;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String toString(){
        String tempString = getName();
        if (getWeight() > 50){
            tempString = tempString + " is big!";
        }
        else{
            tempString = tempString + " is small!";
        }

        return tempString;
    }
}
